// Package to implement a binary search tree

package tree

import (
    "fmt"
    "strings"
)

type Tree struct {
    Root *Node
    Size int
}

type Node struct {
    Name   string
    Height int
    Left   *Node
    Right  *Node
}

func Max(n1, n2 int) int {
    if n1 > n2 {
        return n1
    }

    return n2
}


func (t *Tree) Find(s string, exact bool) (string, error) {
    res, ok := t.Root.find(s, exact)
    if ok {
        return res, nil
    } else {
        return res, fmt.Errorf("Find : Could not find a match for %v", s)
    }
}

func (n *Node) find(s string, exact bool) (string, bool) {
    if n == nil {
        return "", false

    } else if exact && n.Name == s {
        return n.Name, true

    } else if !exact && strings.Contains(n.Name, s) {
        return n.Name, true

    } else if s < n.Name {
        if res, ok := n.Left.find(s, exact); ok {
            return res, true
        }
    } else if s > n.Name {
        if res, ok := n.Right.find(s, exact); ok {
            return res, true
        }
    }
    return "", false
}

func (n *Node) getHeight() int {
    if n == nil {
        return -1
    }
    return Max(n.Left.getHeight(), n.Right.getHeight()) + 1
}

func (n *Node) getBalance() int {

    return n.Left.getHeight() - n.Right.getHeight()
}

func (n *Node) isBalanced() bool {
    bal := n.getBalance()
    if bal > 1 || bal < -1 {
        return false
    }
    return true
}

func (n *Node) rotateRight(c *Node) {
    newC := c.Left
    c.Left = newC.Right
    newC.Right = c
    if c == n.Left {
        n.Left = newC
    } else {
        n.Right = newC
    }
}

func (n *Node) rotateLeft(c *Node) { // reimplement
    newC := c.Right
    c.Right = newC.Left
    newC.Left = c
    if c == n.Left {
        n.Left = newC
    } else {
        n.Right = newC
    }

}

func (n *Node) rotateLeftRight(c *Node) { // reimplement
    c.rotateLeft(c.Left)
    c.Left.Left.Height = c.Left.Left.getHeight()
    c.Left.Height = c.Left.getHeight()
    n.rotateRight(c)
}

func (n *Node) rotateRightLeft(c *Node) { // reimplement
    c.rotateRight(c.Right)
    c.Right.Right.Height = c.Right.Right.getHeight()
    c.Right.Height = c.Right.getHeight()
    n.rotateLeft(c)
}

func (t *Tree) balance() { // reimplement
    if t == nil || t.Root == nil { // Nothing to balance, should never get here
        return
    } else {
        parent := &Node{Name: "Parent-temp", Left: t.Root}
        parent.balance(parent.Left)
        t.Root = parent.Left

    }
}

func (n *Node) balance(c *Node) { // reimplement
    if bal := c.getBalance(); bal > 1 { // leans to the left
        if sbal := c.Left.getBalance(); sbal >= 0 { // leans again to the left or stable. simple case : Rotate Right
            n.rotateRight(c)
            c.Height = c.getHeight()
            n.Height = n.getHeight()
        } else if sbal < 0 { // leans to the opposite side, double rotation required, rotate left then right
            n.rotateLeftRight(c)
            c.Height = c.getHeight()
            n.Height = n.getHeight()

        }
    } else if bal < -1 { // leans to the right
        if sbal := c.Right.getBalance(); sbal <= 0 { // leans again to the right or stable. simple case : Rotate Left
            n.rotateLeft(c)
            c.Height = c.getHeight()
            n.Height = n.getHeight()
        } else if sbal > 0 { // leans to the opposite side, double rotation required, rotate right then left
            n.rotateRightLeft(c)
            c.Height = c.getHeight()
            n.Height = n.getHeight()
        }
    }
}

func (t *Tree) Insert(c *Node) { // OK
    if t.Root == nil {
        t.Root = c
        t.Size++
    } else {
        if ins := t.Root.insert(c); ins { // New node was inserted
            t.Root.Height = t.Root.getHeight()
            t.Size++

            if ok := t.Root.isBalanced(); !ok { // Root node is not balanced
                t.balance()
                t.Root.Height = t.Root.getHeight()
            }
        }
    }
}

func (n *Node) insert(c *Node) bool { // OK
    if n.Name == c.Name { // already exists
        return false

    } else if c.Name < n.Name { // Going to the left

        if n.Left == nil { // Adding new node to the left
            n.Left = c
            n.Height = n.getHeight()

            return true
        } else { //
            if ok := n.Left.insert(c); ok {
                n.Height = n.getHeight()
                if ok := n.Left.isBalanced(); !ok { // tree needs balancing at node n
                    n.balance(n.Left)
                }
                return true
            }
        }

    } else if c.Name > n.Name { // Going to the right
        if n.Right == nil { // Adding new node to the right
            n.Right = c
            n.Height = n.getHeight()
            return true
        } else { //
            if ok := n.Right.insert(c); ok {
                n.Height = n.getHeight()
                if ok := n.Right.isBalanced(); !ok { // tree needs balancing at node n
                    n.balance(n.Right)
                }

                return true
            }
        }

    }

    return false
}

func (t *Tree) Walk() {
    t.Root.walk()
}

func (n *Node) walk() {
    if n == nil {
        return
    }
    n.Left.walk()
    fmt.Printf("%v[%v]%v\n", strings.Repeat(" ", n.Height+1), n.Height, n.Name)
    //n.Left.walk()
    n.Right.walk()

    return
}
